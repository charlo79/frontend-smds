      
<?php  
session_start();
if($_SESSION !=null){
  header("location: ../Pantallas");
}

?>          

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Iniciar</title>
    <link href="../css/Login-Registro.css" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimun-scale=1.0">
</head>
<body>
    
  <div class="contenedorR">
    <div class="container">
        <div class="container-triangulo"></div>
        <h2 class="titulo">Iniciar</h2>
      
        <form class="contenedor" action="../Validaciones/Inicio-Sesion.php" method="POST">
          <p><input type="text" placeholder="Usuario" name="username" id="username"></p>
          <p><input type="password" placeholder="Contraseña" id="password" name="password"></p>
          <p><input type="submit" id="enviar" value="Ingresar" name="InicioSesion"></p>
        </form>



        <div class="container-si-inicia-Registra">
          <p><a class="boton-Login" href = "../Registro/Registro.php" >Crear Cuenta</a>
          </p>
        </div>
      </div>
    </div>

    

</body>
</html>