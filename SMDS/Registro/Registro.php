      
<?php  
session_start();
if($_SESSION !=null){
  header("location: ../Pantallas");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Registro</title>
  <link href="../css/Login-Registro.css" rel="stylesheet" type="text/css">
</head>

<body>

  <div class="container">
    <div class="container-triangulo"></div>
    <h2 class="titulo">Registrar</h2>

    <form class="contenedor" action="http://localhost:3000/auth/register" method="POST">
      <p><input type="text" placeholder="Nombre" name="username"></p>
      <p><input type="email" placeholder="Correo"></p>
      <p><input type="password" placeholder="Contraseña" name="password"></p>
      <p><input type="submit" value="Registrar"></p>
    </form>

    <div class="container-si-inicia-Registra">
      <p><a class="boton-Login" href = "../Login/index.php" >Ya Tengo Una Cuenta</a>
      </p>
    </div>
  </div>
</body>

</html>