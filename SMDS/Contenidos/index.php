<?php  
session_start();
   $user="";
   if($_SESSION['autentificado'] !='SI'){
    header("location: ../Login/index.php");


   }else{
    $user="";
    $user=$_SESSION['usuario'];

   }
?>           


<!DOCTYPE html>
<html lang="es">
<head>
    <title>Contenidos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="Stylesheet" href="../css/header.css" />
    <link rel="Stylesheet" href="../css/style2.css" />

    <style>
        main {
        
          column-count:1;
	        column-gap: 4em;
	        column-rule: 1px solid rgba(187, 187, 187, 0.541);
	        column-width: 750px;
        }

        h1 {
            column-span:all;
        }

        img {
            display:block;
            column-span:all;
        }
    </style>
    <style type="text/css">
      #aviso-movil-horizontal { display: none; }
      @media only screen and (orientation:portrait) {
          #wrapper { display:none; }
          #aviso-movil-horizontal { display:block; }
      }
      @media only screen and (orientation:landscape) {
          #aviso-movil-horizontal { display:none; }
      }
  </style>

</head>
<body>
  
   
    <header>
		<div class="contenedor">
			<nav class="menu">
        <ul style="margin-left: 20%;">
          <li><a href="../Pantallas/">Pantallas</a></li>
          <li><a id="actual">Contenidos</a></li>
          <li><a href="#">Monitor</a></li>
          <div style="margin-left: 50%;">
            <li><a><?php echo $user ?></a></li>
            <li><a href="../Validaciones/Cerrar-Sesion.php">Cerrar Sesion</a></li>
          </div>
        </ul>
			</nav>
		</div>
	</header>



    <main style="background-color: transparent;">
        <p>
           <div class="scrolltblista">
            <table id="tabla">
              <thead>
                <tr>
                  <th>NOMBRE</th>
                  <th>FORMATO</th>
                  <th>DURACIÓN</th>
                  <th>ACCIONES</th>
                </tr>
              </thead>
              <tr>
                <td>Video1</td>
                <td>mp4</td>
                <td>6 hrs</td>
                <td>
                  <div class="btn-play play"><img src="../src/iconoplay.png" alt="star" class="width"></div>
                  <div class="btn-play editar"><a href="../Detalles/"> <img src="../src/iconoeditar.png" class="width">
                    </a></div>
                </td>
              </tr>

              <tr>
                <td>Video2</td>
                <td>mp4</td>
                <td>30 min</td>
                <td>
                  <div class="btn-play play"><img src="../src/iconoplay.png" alt="star" class="width"></div>
                  <div class="btn-play editar"><a href="../Detalles/"> <img src="../src/iconoeditar.png" class="width">
                    </a></div>
                </td>
              </tr>

            </table>

            <div class="custom-input-file col-md-6 col-sm-6 col-xs-6">
              <input type="file" id="fichero-tarifas" class="input-file" value="">
              Subir Contenido...
            </div>



         </div>
          </p>
  </main>
</body>
</html>
