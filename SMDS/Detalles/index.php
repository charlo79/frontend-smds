<?php  
session_start();
   $user="";
   if($_SESSION['autentificado'] !='SI'){
    header("location: ../Login/index.php");


   }else{
    $user=$_SESSION['usuario'];

   }
?>           


<!DOCTYPE html>
<html lang="es">
<head>
    <title>Detalles</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="Stylesheet" href="../css/header.css" />
    <link rel="Stylesheet" href="../css/style2.css" />
    <link rel="Stylesheet" href="../css/emergente.css" />

    <style>
        main {
        
          column-count:2;
	        column-gap: 3em;
	        column-rule: 1px solid #bbb;
	        column-width: 240px;
        }

        h1 {
            column-span:all;
        }

        img {
            display:block;
            column-span:all;
        }
    </style>

</head>
<body>

  <header>
    <div class="contenedor">
      <nav class="menu">
        <ul style="margin-left: 20%;">
          <li><a href="../Pantallas/">Volver</a></li>
          <li><a id="actual" class="pantallanm">Pantalla Aaa02</a></li>
          <div style="margin-left: 50%;">
            <li><a><?php echo $user ?></a></li>
            <li><a href="../Validaciones/Cerrar-Sesion.php">Cerrar Sesion</a></li>
          </div>
        </ul>
      </nav>
    </div>
  </header>



  <main>
    <p>
    <div>
      <div class="scrolltblista">
        <table>
          <thead>
            <tr>
              <th>Lista</th>
              <th>Duación</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tr>
            <td>Promo 2</td>
            <td>4 hrs</td>
            <td>
              <div class="btn-play Ver"><img src="../src/iconoeditar.png" alt="star" class="width"></div>
            </td>
          </tr>
        </table>
      </div>

      
    </div>
    </p>

    <br><br><input type="checkbox" id="btnc-modal">
      <label for="btnc-modal" class="lblc-modal">AÑADIR LISTA</label>

      <p class="PrograCont">
    <div id="columna2" class="PrograCont">
      <p style="color: transparent;">-</p>
      <div>
        <video id="idvideo" controls poster="../src/vistaprevia.png">
          <source src="../src/video2.mp4" type="video/mp4">
        </video>
      </div>
      <div class="btns-programacion">

        <div class="row">
          <div class="col-md-82">

            <div class="cols">
              <div class="col1">
                <p>
                  PROGRAMACIÓN<br>
                  <select class="caja" id="programacion">
                    <option value="" disabled selected>Selecciona ---</option>
                    <option value="DEFECTO">POR DEFECTO</option>
                  </select>
                </p>

                <p style="margin-top: 20%;">
                  LISTA DE REPRODUCCIÓN
                  <br>
                  <select class="caja" id="lista">
                    <option value="" disabled selected>Selecciona ---</option>
                    <option value="DEFECTO">POR DEFECTO</option>
                  </select>
                </p>
              </div>
              <div class="col2">
                <p>
                  Inicio<br>
                  <input type="date" id="start" name="trip-start" value="2021-08-30" min="2021-08-30" max="2022-08-30">
                  <input type="time" name="eta">
                </p>
                <p style="margin-top: 20%;">
                  FIN <br>
                  <input type="date" id="start" name="trip-start" value="2021-08-30" min="2021-08-30" max="2022-08-30">
                  <input type="time" name="eta">
                </p>



                <p>
                  <br>
                  <div class="dias">
                  <label>D</label>
                  <label>L</label>
                  <label>M</label>
                  <label>M</label>
                  <label>J</label>
                  <label>V</label>
                  <label>S</label>
                  <br>                  </div>

                  <div class="checboxdias">

                  <label><input type="checkbox" name="dias"> </label>
                  <label><input type="checkbox" name="dias"> </label>
                  <label><input type="checkbox" name="dias"> </label>
                  <label><input type="checkbox" name="dias"></label>
                  <label><input type="checkbox" name="dias"> </label>
                  <label> <input type="checkbox" name="dias"> </label>
                  </div>
                </p>  
              </div>



            </div>

          </div>

        </div>

      </div>
      <br><br>
      <br>
      <label class="btnGuardar">Guardar Programación</label>
    </div>
    </p>






      <div class="modal">
        <div class="modal1" id="ventana-agregar">
          <div class="content">
            <form class="contenedor">
              <p><input type="text" class="Nombre" placeholder="Nombre Lista"></p>
              <div id="columnaE">
                <div class="atributos">
                  <table id="tbl-lista" class="disponible">
                    <thead>
                      <tr>
                        <th class="vd">Videos Disponibles</th>
                        <th>-</th>
                      </tr>
                    </thead>
                    <tr>
                      <td>Aaa002</td>
                      <th>
                        <div class="btn-play play"><img src="../src/agregar.png" alt="star" class="width"></td>
                    </tr>
                  </table>
                </div>
              </div>



              <div id="columnaE">
                <div class="t2">
                  <div class="atributos">
                    <table id="tbl-lista">
                      <thead>
                        <tr>
                          <th>En Mi Lista</th>
                          <th>Repeticiones</th>
                          <th>x</th>
                        </tr>
                      </thead>
                      <tr>
                        <td>Aaa002</td>
                        <td><input type="number" value="1" min="1" max="10"></td>
                        <td>
                          <div class="btn-play play"><img src="../src/eliminar.webp" alt="star" class="width">
                        </td>
                      </tr>
                     </table>
                  </div>
                </div>
              </div>

              <p><input type="submit" class="guardar" id="guardarM" value="Guardar"></p>
            </form>
          </div>
        </div>
      </div>

   
  </main>

  <script src="detalles.js"></script>

  
</body>

</html>