
<?php  
session_start();
$user=null;
if($_SESSION['autentificado'] !='SI'){
 header("location: ../Login/index.php");


}else{
 $user=$_SESSION['usuario'];


 $fechaGuardada = $_SESSION["ultimoAcceso"];
 $ahora = date("Y-n-j H:i:s");
 $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));

 //comparamos el tiempo transcurrido
  if($tiempo_transcurrido >= 600) {
  //si pasaron 10 minutos o más
   session_destroy(); // destruyo la sesión
   header("location: ../Login/index.php"); //envío al usuario a la pag. de autenticación
   //sino, actualizo la fecha de la sesión
 }else {
 $_SESSION["ultimoAcceso"] = $ahora;
}
}
?> 

}
?>               



<!DOCTYPE html>
<html lang="es">
<head>
    <title>Detalles</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="Stylesheet" href="../css/header.css" />
    <link rel="Stylesheet" href="../css/style2.css" />

    <style>
        main {
        
          column-count:1;
	        column-gap: 4em;
	        column-rule: 1px solid rgba(187, 187, 187, 0.541);
	        column-width: 750px;
        }

        h1 {
            column-span:all;
        }

        img {
            display:block;
            column-span:all;
        }
    </style>
    <style type="text/css">
      #aviso-movil-horizontal { display: none; }
      @media only screen and (orientation:portrait) {
          #wrapper { display:none; }
          #aviso-movil-horizontal { display:block; }
      }
      @media only screen and (orientation:landscape) {
          #aviso-movil-horizontal { display:none; }
      }
  </style>

</head>
<body>
  
   
    <header>
		<div class="contenedor">
			<nav class="menu">
        <ul style="margin-left: 20%;">
          <li><a id="actual">Pantallas</a></li>
          <li><a href="../Contenidos">Contenidos</a></li>
          <li><a href="#">Monitor</a></li>
          <div style="margin-left: 50%;">
            <li><a ><?php echo $user ?></a></li>
            <li><a href="../Validaciones/Cerrar-Sesion.php">Cerrar Sesion</a></li>
          </div>
        </ul>
			</nav>
		</div>
	</header>



    <main style="background-color: transparent;">
        <p>
           <div class="scrolltblista2" >
            <table  id="tabla" class="pantallasDN">
              <thead>
                <tr>
                  <th>CLAVE</th>
                  <th>ULTIMA CONEXIÓN</th>
                  <th>Reproducción Actual</th>
                  <th>UBICACIÓN</th>
                  <th>ACCIONES</th>
                </tr>
              </thead>
              <tr>
    
             </tr>
            
            </table>
            </div>
          </p>
  </main>

  <script src="index.js"></script>
  <script src="../socket.io.js"></script>
  <script src="../main.js"></script>
  

</body>
</html>
